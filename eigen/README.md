# 

# Install

dependencies

- unix
- `python3` `python3-numpy` `python3-scipy` `python3-leveldb`
- `gensim` (via `easy_install3`)

# Data

1. `incoming/*` downloaded raw data
2. `extract/materials.freq.gz` ids of materials ordered by number of loans, truncation yield most popular materials
3. `dbs/id_mid` `dbs/mid_id` mapping between material ids (0,1,2,..) and long ids (a la '870970-basis:27889107')
4. `extract/lid_mid.lst.gz` mapping from loan-id to materials loaned. This is intermediate data.
5. `extract/mid_lid.lst.gz` mapping from the n most popular materials loaned to sequential loan ids (0,1,2,...)
6. `dbs/mid_meta` mapping from mid to metadata
7. `extract/idx_mid.lst.gz`, `dbs/idx_mid.leveldb` mapping from term to meta data (for searching)

# Future work

Use laanerid+year instead of just laanerid

