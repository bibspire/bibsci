#include <stdlib.h>
#include <stdio.h>
#include "leveldb/db.h"
#include <iostream>
#include <math.h>
using namespace std;

#define MAX_SIZE 1000000

int line[MAX_SIZE];
int line_len = 0;

int icmp(const void *a, const void *b) {
  return *((int *)a) - *((int *)b);
}

int main(int argc, char** argv) {

  leveldb::DB* db;
  leveldb::Options options;
  options.create_if_missing = true;
  leveldb::Status s = leveldb::DB::Open(options, "dbs/mid_lids.leveldb", &db);
  if (!s.ok()) cerr << s.ToString() << endl;

  int dim = atoi(argv[1]);
  printf("dim %d\n", dim);
  
  int i, j;
  int prev_i = 0;
  while(2 == scanf("%d %d", &i, &j)) {
    while(i > prev_i) {
      qsort(line, line_len, sizeof(int), icmp);
      //if(1 == (i % 100)) printf("%d %d -- %d %d %d %d\n", prev_i, line_len, line[0], line[1], line[2], line[3]);
      
      s = db->Put(leveldb::WriteOptions(), leveldb::Slice((char *)&prev_i, 4), leveldb::Slice((char *)line, 4 * line_len));
      if (!s.ok()) cerr << s.ToString() << endl;
      line_len = 0;
      ++prev_i;
    }
    assert(line_len < MAX_SIZE);
    line[line_len] = j;
    ++line_len;
  }

  string i_row;
  string j_row;
  FILE *fp = fopen("out/covar.bin", "wb");
  for(i = 0; i < dim; ++i) {
    s = db->Get(leveldb::ReadOptions(), leveldb::Slice((char *)&i, 4), &i_row);
    if (!s.ok()) {
      i_row.clear();
    }
    for(j = 0; j < dim; ++j) {
      s = db->Get(leveldb::ReadOptions(), leveldb::Slice((char *)&j, 4), &j_row);
      if (!s.ok()) {
        j_row.clear();
      }
      int *i_data = (int *)i_row.data();
      int *j_data = (int *)j_row.data();
      int i_len = i_row.size() / 4;
      int j_len = j_row.size() / 4;
      double sum = 0;
      int j_i = 0;
      for(int i_i = 0; i_i < i_len; ++i_i) {
        while(j_i < j_len && j_data[j_i] < i_data[i_i]) {
          ++j_i;
        }

          if(i_data[i_i] == j_data[j_i]) {
            //sum += sqrt(1.0 / i_len / j_len);
            sum += 1.0 / sqrt(i_len) / sqrt(j_len);
          }
      }
      fwrite(&sum, sizeof(double), 1, fp);
      //printf("%d %d %f\n", i, j, sum);
      //cout << i << "(" << i_len << ") " << j << " " << sum << endl;
      // printf("%d(%d), %d(%d)\n", i, i_row.size() / 4, j, j_row.size() / 4);
    }
    printf("%d\n", i);
  }
  fclose(fp);
  delete db;

}

