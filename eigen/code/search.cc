#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <assert.h>


#define MATERIAL_COUNT 10000
#define DIMENSIONS 128

float dist2(float *a, float *b) {
  float sum = 0;
  for(int i = 0; i < 10; ++i) {
    float diff = a[i] - b[i];
    sum += diff * diff;
  }
  return sum;
}

int main(int argc, char **argv) {
  int fd = open("../out/matrix.bin", O_RDONLY);
  assert(fd);
  float *matrix = (float *)mmap(NULL, MATERIAL_COUNT * sizeof(float) * DIMENSIONS, PROT_READ, MAP_PRIVATE, fd, 0);
  assert(matrix);

  for(int i = 0; i < MATERIAL_COUNT; ++i) {
    printf("%f %d\n", (double) dist2(matrix + DIMENSIONS * 1234, matrix + DIMENSIONS * i), i);
  }
  return EXIT_SUCCESS;
}
