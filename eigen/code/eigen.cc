#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <assert.h>
#include <math.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <eigen3/Eigen/Dense>

using namespace std;
using namespace Eigen;

int main(int argc, char** argv) {

  int dim = atoi(argv[1]);

  int fd = open("out/covar.bin", O_RDONLY);
  double *covar = (double *)mmap(NULL, 10000 * 10000 * sizeof(double), PROT_READ, MAP_PRIVATE, fd, 0);
  assert(covar);

  MatrixXf m(dim, dim);

  for(int i = 0; i < dim; ++i) {
    for(int j = 0; j < dim; ++j) {
      m(i,j) = covar[i + 10000 * j];
      //printf("%d %d %f\n", i, j, covar[i + 10000 * j]);
    }
  }
  printf("initialised matrix\n");


  SelfAdjointEigenSolver<MatrixXf> es(m);
  printf("created eigensolver\n");

  VectorXf vals = es.eigenvalues();
  FILE *fp = fopen("out/eigen.values", "w");
  for(int i = 0; i < dim; ++i) {
    fprintf(fp, "%e\n",vals(i));
  }
  printf("wrote out/eigen.values\n");

  MatrixXf vectors = es.eigenvectors();
  fclose(fp);
  fp = fopen("out/eigen.bin", "wb");
  for(int i = 0; i < dim; ++i) {
    for(int j = 0; j < dim; ++j) {
      double d = vectors(i,j);
      fwrite(&d, sizeof(double), 1, fp);
    }
  }
  fclose(fp);
  printf("wrote out/eigen.bin\n");


  return EXIT_SUCCESS;
}

