#include <stdlib.h>
#include <stdio.h>
#include "leveldb/db.h"
#include "json.hpp"
#include <iostream>
#include <math.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

using namespace std;
using json = nlohmann::json;

int main(int argc, char** argv) {

  leveldb::DB* db;
  leveldb::Options options;
  options.create_if_missing = false;
  leveldb::Status s = leveldb::DB::Open(options, "../dbs/mid_meta.leveldb", &db);
  if (!s.ok()) {
    cerr << s.ToString() << endl;
    return -1;
  }

  int mid = atoi(argv[1]);
  string json_str;
  //s = db->Get(leveldb::ReadOptions(), leveldb::Slice((char *)&mid, sizeof(int)), &json_str);
  //if (!s.ok()) { cerr << s.ToString() << endl;}
  //cout << json::parse(json_str)["title"] << endl;
  FILE *fp = fopen("../out/covar.bin", "rb");
  int fd = open("../out/covar.bin", O_RDONLY);
  double *covar = (double *)mmap(NULL, 10000 * 10000 * sizeof(double), PROT_READ, MAP_PRIVATE, fd, 0);
  assert(covar);
  for(int i = 0; i < 10000; ++i) {
    int bi = __builtin_bswap32(i);
    s = db->Get(leveldb::ReadOptions(), leveldb::Slice((char *)&bi, sizeof(int)), &json_str);
    if (!s.ok()) { cerr << s.ToString() << i << endl;}
    cout << covar[i + 10000 * mid] << " " << json::parse(json_str)["title"] << " " << i << endl;
  }
}

