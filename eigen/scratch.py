import gensim
import gzip
import struct
import numpy

#  1001
#  1101
#  0110
#  1111
#  0011
#  0000
#  1111

class Corpus(object):
    def __iter__(self):
        yield [(0,1), (3,1)]
        yield [(0,1), (1,1), (3,1)]
        yield [(1,1), (2,1)]
        yield [(0,1), (1,1), (2,1), (3,1)]
        yield []
        yield [(0,1), (2,1)]

print("making model")
lsi = gensim.models.LsiModel(Corpus(), num_topics=128)
print("calculating matrix")
V = gensim.matutils.corpus2dense(lsi[Corpus()], len(lsi.projection.s)).T / lsi.projection.s
print(V.shape)
print(V[0])
print("writing matrix")
out = open('out/matrix.bin', 'wb')
for v in V:
    v = v / numpy.linalg.norm(v)
    for f in v:
        out.write(struct.pack("f", f))
