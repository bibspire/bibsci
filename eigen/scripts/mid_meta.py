import gzip
import json
import leveldb

id_mid = leveldb.LevelDB('dbs/id_mid.leveldb')
mid_meta = leveldb.LevelDB('dbs/mid_meta.leveldb')

for line in gzip.open('incoming/materialer-used.json.gz'):
    obj = json.loads(line.decode('utf-8'));
    try:
        mid = id_mid.Get(obj['_id'].encode('utf-8'))
    except:
        continue
    mid_meta.Put(mid, line)
