import gzip
import json
import leveldb

mid_meta = leveldb.LevelDB('dbs/mid_meta.leveldb')
prev = False

for line in gzip.open('extract/mid_lid.lst.gz'):
    mid = int(line.decode('utf-8').strip().split(' ')[0])
    if prev == mid:
        continue
    prev = mid
    meta = json.loads(mid_meta.Get(mid.to_bytes(4, byteorder='big')).decode('utf-8'))
    terms = meta.get('subject-term', [])
    terms.append(meta.get('title', 'sk'))
    terms.append(meta.get('creator', 'sk'))
    for term in terms:
        if term == 'sk' or term == 'Skønlitteratur' or term == '÷' or term == '!':
            continue
        print('%s%8d' % (term, mid))
