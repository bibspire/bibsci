import gzip
import json
import leveldb

id_mid = leveldb.LevelDB('dbs/id_mid.leveldb')

for line in gzip.open('incoming/udlaan.json.gz'):
    line = json.loads(line.decode('utf-8'));
    lid = int(line['laaner_id'])
    mid = int.from_bytes(id_mid.Get(line['materiale_id'].encode('utf-8')), byteorder='big')
    print(lid, mid)
