import gzip
import json
import leveldb

prev = 'sk'
ids = []
idx_mid = leveldb.LevelDB('dbs/idx_mid.leveldb')

for line in gzip.open('extract/idx_mid.lst.gz'):
    line = line.decode('utf-8').strip()
    mid = int(line[-8:])
    term = line[:-8]
    if term != prev:
        idx_mid.Put(term.encode('utf-8'), b''.join([i.to_bytes(4, byteorder='big') for i in ids]))
        prev = term
        ids = []
    ids.append(mid)
idx_mid.Put(term.encode('utf-8'), b''.join([i.to_bytes(4, byteorder='big') for i in ids]))
