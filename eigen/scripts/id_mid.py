import gzip
import leveldb

id_mid = leveldb.LevelDB('./dbs/id_mid.leveldb')
mid_id = leveldb.LevelDB('./dbs/mid_id.leveldb')

mid = 0
for line in gzip.open('extract/materials.freq.gz'):
    id = line.decode('utf-8').strip().split(' ')[1].encode('utf-8')
    mid_bytes = mid.to_bytes(4, byteorder='big')
    id_mid.Put(id, mid_bytes)
    mid_id.Put(mid_bytes, id)
    mid += 1
