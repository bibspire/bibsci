import os
import gzip
import json

max_mid = int(os.environ['MID_DIM'])
prev = 0
mids = []
j = 0
for line in gzip.open('extract/lid_mid.lst.gz'):
    line = line.decode('utf-8').split(' ');
    lid = int(line[0])
    mid = int(line[1])
    if mid > max_mid:
        continue
    if lid != prev:
        if len(mids) > 1:
            for k in mids:
                print(k, j)
            j += 1
        mids = []
        prev = lid
    mids.append(mid)
