import gensim
import gzip
import struct
import numpy

class Corpus(object):
    def __iter__(self):
        prev = 0
        ids = []
        for line in gzip.open('extract/mid_lid.lst.gz'):
            line = line.decode('utf-8').split(' ');
            mid = int(line[0])
            lid = int(line[1])
            if prev != mid:
                yield [(i, 1) for i in ids]
                prev = mid
                ids = []
            ids.append(lid)
        yield [(i, 1) for i in ids]

print("making model")
lsi = gensim.models.LsiModel(Corpus(), num_topics=128)
print("calculating matrix")
V = gensim.matutils.corpus2dense(lsi[Corpus()], len(lsi.projection.s)).T / lsi.projection.s
print(V.shape)
print(V[0])
print("writing matrix")
out = open('out/matrix.bin', 'wb')
for v in V:
    v = v / numpy.linalg.norm(v)
    for f in v:
        out.write(struct.pack("f", f))
