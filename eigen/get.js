var level = require('level')
var {promisify} = require('util');


db = level('./dbs/mid_meta.leveldb');

get = (k) => new Promise((resolve, reject) => db.get(k, (err, result) => err ? reject(err) : resolve(result)))
k = Buffer.from([0,0,0,7]);
async function main () {
  var b = Buffer.alloc(4);
  b.writeInt32BE(+process.argv[process.argv.length - 1], 0)
  console.log(await get(b))
}
main();
