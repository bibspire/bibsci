install -d covers
for x in `cat incoming/materialer-used.json | sed -e 's/^.."_id" . "//' | sed -e 's/".*//' | sort -R | uniq`
do
  if [ ! -f "covers/$x.json.gz" ]; then
    curl -s "https://openplatform.dbc.dk/v2/work?access_token=25dcf1e15e318e974f30dae8e710ab4f1bdd8324&fields=\[%22coverDataUrlFull%22\]&pids=\[%22$x%22\]" > "covers/$x.json"
    gzip -9  "covers/$x.json"
    ls -l "covers/$x.json.gz"
  fi
done
