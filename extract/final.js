const metaeigen = require('./metaeigen.json');
const a = new Array(10000);
for(const o of metaeigen) {
  a[o.idx] = o;
}
const recs = [];
const fs = require('fs');
for(let i = 0; i < 10000; ++i) {
  const o = a[i];
  if(o) {
    let rec = JSON.parse(fs.readFileSync('./rec/recommend' + i + '.json'));
    rec = rec.slice(1).filter(([score, id]) => a[id])
    rec = rec.map(([score, id]) => [a[id].i, ((score * 100000) | 0) / 100000]).slice(0,50);
    const result = {
      id: o.i,
      creator: o.creator || '',
      title: o.title || '',
      tags: (o['subject-term']||[]).filter(s => !(['sk', '!', '÷'].includes(s))),
      related: rec
    }
    recs.push(result);
    console.log(recs.length)
  }
}
fs.writeFileSync('data.json', JSON.stringify(recs), 'utf-8');
